﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Pomelo.EntityFrameworkCore.MySql.Infrastructure;
using ZipAssessment.Repository.Repository;

namespace ZipAssessment.Repository
{
    public static class DependencyInjection
    {
        public static IServiceCollection AddRepository(this IServiceCollection services, IConfiguration configuration)
        {
            services.AddDbContextPool<ZipAccountContext>(
                options => options.UseMySql(configuration.GetConnectionString("DefaultConnection"), 
                    mySqlOptions =>
                    {
                        mySqlOptions.ServerVersion(new Version(5, 7, 17), ServerType.MySql);
                    }
                ));

            services.AddScoped<IZipAccountContext>(pro => pro.GetService<ZipAccountContext>());

            return services;
        }
    }
}
