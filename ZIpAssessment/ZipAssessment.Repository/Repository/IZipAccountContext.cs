﻿using System.Threading;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using ZipAssessment.Repository.Entity;

namespace ZipAssessment.Repository.Repository
{
    public interface IZipAccountContext
    {
        DbSet<User> Users { get; set; }

        DbSet<Account> Accounts { get; set; }

        Task<int> SaveChangesAsync(bool acceptAllChangesOnSuccess,
            CancellationToken cancellationToken = new CancellationToken());
    }
}