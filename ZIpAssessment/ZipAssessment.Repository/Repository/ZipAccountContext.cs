﻿using System;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using ZipAssessment.Repository.Entity;

namespace ZipAssessment.Repository.Repository
{
    public class ZipAccountContext : DbContext, IZipAccountContext
    {
        public ZipAccountContext(DbContextOptions<ZipAccountContext> options)
            : base(options)
        {
        }

        public virtual DbSet<User> Users { get; set; }

        public virtual DbSet<Account> Accounts { get; set; }

        public override Task<int> SaveChangesAsync(CancellationToken cancellationToken = new CancellationToken())
        {
            foreach (var entity in ChangeTracker.Entries<AuditRecord>())
            {
                switch (entity.State)
                {
                    case EntityState.Added:
                        //normal set user here
                        entity.Entity.Created = DateTime.Now;
                        break;
                    case EntityState.Modified:
                        //normal set user here
                        entity.Entity.LastModified = DateTime.Now;
                        break;
                }
            }
            return base.SaveChangesAsync(cancellationToken);
        }

        public override Task<int> SaveChangesAsync(bool acceptAllChangesOnSuccess, CancellationToken cancellationToken = new CancellationToken())
        {
            foreach (var entity in ChangeTracker.Entries<AuditRecord>())
            {
                switch (entity.State)
                {
                    case EntityState.Added:
                        //normal set user here
                        entity.Entity.Created = DateTime.Now;
                        break;
                    case EntityState.Modified:
                        //normal set user here
                        entity.Entity.LastModified = DateTime.Now;
                        break;
                }
            }
            return base.SaveChangesAsync(acceptAllChangesOnSuccess, cancellationToken);
        }
    }
}
