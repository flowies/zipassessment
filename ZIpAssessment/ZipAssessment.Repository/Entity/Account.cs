﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace ZipAssessment.Repository.Entity
{
    public class Account : AuditRecord
    {

        public Account()
        {
            ExternalId = Guid.NewGuid();
        }
        [Key]
        public int Id { get; set; }
        public Guid ExternalId { get; set; }
        public virtual User User { get; set; }

        public double CurrentBalance { get; set; }
        public double Limit { get; set; }
        public bool AccountOverdue { get; set; }


    }
}
