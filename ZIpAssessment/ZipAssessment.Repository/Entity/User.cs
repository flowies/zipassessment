﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using ZipAssessment.Repository.Parameters;

namespace ZipAssessment.Repository.Entity
{
    public class User : AuditRecord
    {
        protected User()
        {

        }

        [Key] public int Id { get; set; }
        public Guid ExternalId { get; set; }

        public string FirstName { get; set; }
        public string LastName { get; set; }
        public double MonthlySalary { get; set; }
        public double MonthlyExpenses { get; set; }
        public string EmailAddress { get; set; }

        public virtual ICollection<Account> Account { get; set; }

        public static User CreateUser(CreateUser parameter)
        {
            return new User
            {
                ExternalId = Guid.NewGuid(),
                FirstName = parameter.FirstName,
                LastName = parameter.LastName,
                MonthlyExpenses = parameter.MonthlyExpenses,
                MonthlySalary = parameter.MonthlySalary,
                EmailAddress = parameter.EmailAddress,
                Account = new List<Account>()
            };
        }

        public void UpdateUser(EditUser value)
        {
            FirstName = value.FirstName;
            LastName = value.LastName;
            MonthlyExpenses = value.MonthlyExpenses;
            MonthlySalary = value.MonthlySalary;
        }
    }
}
