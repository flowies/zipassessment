﻿using ZipAssessment.BLL.Mapping;

namespace ZipAssessment.BLL.InternalParameters
{
    public class CreateUser : IMapFrom<Repository.Parameters.CreateUser>
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public double MonthlySalary { get; set; }
        public double MonthlyExpenses { get; set; }
        public string EmailAddress { get; set; }
    }
}
