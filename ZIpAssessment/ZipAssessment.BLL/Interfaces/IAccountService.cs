﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using ZipAssessment.BLL.InternalModels;

namespace ZipAssessment.BLL.Interfaces
{
    public interface IAccountService
    {
        Task<List<Account>> GetAllAccounts(CancellationToken cancellationToken);
        Task<Account> GetAccountByExternalId(Guid id, CancellationToken cancellationToken);
        Task<Account> CreateNewAccount(Guid externalUserId, CancellationToken cancellationToken);
    }
}
