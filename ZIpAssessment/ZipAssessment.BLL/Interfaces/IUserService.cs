﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using ZipAssessment.BLL.InternalModels;
using ZipAssessment.BLL.InternalParameters;

namespace ZipAssessment.BLL.Interfaces
{
    public interface IUserService
    {
        Task<List<User>> GetAllUsers(CancellationToken cancellationToken);
        Task<User> GetUserByExternalId(Guid externalId, CancellationToken cancellationToken);
        Task<User> CreateNewUser(CreateUser value, CancellationToken cancellationToken);
        Task<User> EditUser(Guid externalId, EditUser value, CancellationToken cancellationToken);
        Task DeleteUser(Guid externalId, CancellationToken cancellationToken);
    }
}
