﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using AutoMapper.QueryableExtensions;
using Microsoft.EntityFrameworkCore;
using ZipAssessment.BLL.Exceptions;
using ZipAssessment.BLL.Interfaces;
using ZipAssessment.BLL.InternalModels;
using ZipAssessment.Repository.Repository;

namespace ZipAssessment.BLL.Services
{
    public class AccountService : IAccountService
    {

        private readonly IZipAccountContext _dbContext;
        private readonly IMapper _mapper;


        public AccountService(IZipAccountContext dbContext, IMapper mapper)
        {
            _dbContext = dbContext;
            _mapper = mapper;
        }
        public async Task<List<Account>> GetAllAccounts(CancellationToken cancellationToken)
        {
            return await _dbContext.Accounts.ProjectTo<Account>(_mapper.ConfigurationProvider).ToListAsync(cancellationToken);
        }

        public async Task<Account> GetAccountByExternalId(Guid externalId, CancellationToken cancellationToken)
        {
            var account = await _dbContext.Accounts.ProjectTo<Account>(_mapper.ConfigurationProvider)
                .FirstOrDefaultAsync(p => p.ExternalId == externalId, cancellationToken);

            if (account == null)
            {
                throw new NotFoundException();
            }

            return account;
        }

        public async Task<Account> CreateNewAccount(Guid externalUserId, CancellationToken cancellationToken)
        {
            var user = await _dbContext.Users.Include(p=>p.Account)
                .FirstOrDefaultAsync(p => p.ExternalId == externalUserId, cancellationToken);

            if (user == null)
            {
                throw new NotFoundException();
            }

            if (user.MonthlySalary - user.MonthlyExpenses < 1000)
            {
                throw new ValidationIssueException("Not eligible for an account");
            }

            var newAccount = new Repository.Entity.Account();
            if (user.Account == null)
            {
                user.Account = new List<Repository.Entity.Account>();
            }
            user.Account.Add(newAccount);
            await _dbContext.SaveChangesAsync(true, cancellationToken);
            return _mapper.Map<Account>(newAccount);
        }
    }
}
