﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using AutoMapper.QueryableExtensions;
using Microsoft.EntityFrameworkCore;
using ZipAssessment.BLL.Exceptions;
using ZipAssessment.BLL.Interfaces;
using ZipAssessment.BLL.InternalModels;
using ZipAssessment.BLL.InternalParameters;
using ZipAssessment.Repository.Repository;

namespace ZipAssessment.BLL.Services
{
    public class UserService : IUserService
    {
        private readonly IZipAccountContext _dbContext;
        private readonly IMapper _mapper;


        public UserService(IZipAccountContext dbContext, IMapper mapper)
        {
            _dbContext = dbContext;
            _mapper = mapper;
        }

        public async Task<List<User>> GetAllUsers(CancellationToken cancellationToken)
        {
            return await _dbContext.Users.ProjectTo<User>(_mapper.ConfigurationProvider).ToListAsync(cancellationToken);
        }

        public async Task<User> GetUserByExternalId(Guid externalId, CancellationToken cancellationToken)
        {
            var user= await _dbContext.Users.ProjectTo<User>(_mapper.ConfigurationProvider)
                .FirstOrDefaultAsync(p => p.ExternalId == externalId, cancellationToken);

            if (user == null)
            {
                throw new NotFoundException();
            }

            return user;
        }

        public async Task<User> CreateNewUser(CreateUser value, CancellationToken cancellationToken)
        {
            if (value == null)
            {
                throw new ValidationIssueException();
            }
            if (value.MonthlySalary <= 0)
            {
                throw new ValidationIssueException("Salary must be positive");
            }
            if (value.MonthlyExpenses <= 0)
            {
                throw new ValidationIssueException("Expenses must be positive");
            }

            if (await _dbContext.Users.AnyAsync(p => p.EmailAddress == value.EmailAddress, cancellationToken))
            {
                throw new ItemAlreadyExistsException();
            }
            var newUser = Repository.Entity.User.CreateUser(_mapper.Map<Repository.Parameters.CreateUser>(value));
            await _dbContext.Users.AddAsync(newUser, cancellationToken);
            await _dbContext.SaveChangesAsync(true, cancellationToken);
            return _mapper.Map<User>(newUser);
        }

        public async Task<User> EditUser(Guid externalId, EditUser value, CancellationToken cancellationToken)
        {
            if (value == null)
            {
                throw new ValidationIssueException();
            }

            if (await _dbContext.Users.AnyAsync(p => p.EmailAddress == value.EmailAddress && p.ExternalId != externalId, cancellationToken))
            {
                throw new ItemAlreadyExistsException();
            }

            var user = await _dbContext.Users.FirstOrDefaultAsync(p => p.ExternalId == externalId, cancellationToken);

            if (user == null)
            {
                throw new NotFoundException();
            }
            user.UpdateUser(_mapper.Map<Repository.Parameters.EditUser>(value));
            await _dbContext.SaveChangesAsync(true, cancellationToken);
            return _mapper.Map<User>(user);
        }

        public async Task DeleteUser(Guid externalId, CancellationToken cancellationToken)
        {
            var user = await _dbContext.Users.FirstOrDefaultAsync(p => p.ExternalId == externalId, cancellationToken);

            if (user == null)
            {
                throw new NotFoundException();
            }

            _dbContext.Users.Remove(user);
            await _dbContext.SaveChangesAsync(true, cancellationToken);
        }
    }
}
