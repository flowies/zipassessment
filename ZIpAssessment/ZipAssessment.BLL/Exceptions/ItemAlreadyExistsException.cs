﻿using System.Net;

namespace ZipAssessment.BLL.Exceptions
{
    public class ItemAlreadyExistsException : ZipException
    {
        public ItemAlreadyExistsException(string message = "") : base(HttpStatusCode.Conflict, message) { }
    }
}