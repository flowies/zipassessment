﻿using System;
using System.Net;

namespace ZipAssessment.BLL.Exceptions
{
    public class ZipException : Exception
    {
        public HttpStatusCode StatusCode { get; private set; }

        protected ZipException(HttpStatusCode statusCode, string message = "") : base(message)
        {
            StatusCode = statusCode;
        }
    }
}

