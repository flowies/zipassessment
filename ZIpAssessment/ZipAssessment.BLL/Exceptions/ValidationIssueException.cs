﻿using System.Net;

namespace ZipAssessment.BLL.Exceptions
{
    public class ValidationIssueException : ZipException
    {
        public ValidationIssueException(string message = "") : base(HttpStatusCode.BadRequest, message) { }
    }
}