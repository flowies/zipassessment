﻿using System.Net;

namespace ZipAssessment.BLL.Exceptions
{
    public class NotFoundException : ZipException
    {
        public NotFoundException(string message = "") : base(HttpStatusCode.NotFound, message) { }
    }
}