﻿using System;
using ZipAssessment.BLL.Mapping;

namespace ZipAssessment.BLL.InternalModels
{
    public class User : AuditRecord, IMapFrom<Repository.Entity.User>
    {
        public int Id { get; set; }
        public Guid ExternalId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public double MonthlySalary { get; set; }
        public double MonthlyExpenses { get; set; }

        public string EmailAddress { get; set; }
    }
}
