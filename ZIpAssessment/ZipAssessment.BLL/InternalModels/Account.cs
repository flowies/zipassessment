﻿using System;
using ZipAssessment.BLL.Mapping;

namespace ZipAssessment.BLL.InternalModels
{
    public class Account : AuditRecord, IMapFrom<Repository.Entity.Account>
    {
        public int Id { get; set; }
        public Guid ExternalId { get; set; }
        public virtual Repository.Entity.User User { get; set; }

        public double CurrentBalance { get; set; }
        public double Limit { get; set; }
        public bool AccountOverdue { get; set; }
    }
}