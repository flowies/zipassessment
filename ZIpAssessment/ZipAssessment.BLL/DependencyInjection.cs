﻿using System.Reflection;
using AutoMapper;
using Microsoft.Extensions.DependencyInjection;
using ZipAssessment.BLL.Interfaces;
using ZipAssessment.BLL.Services;


namespace ZipAssessment.BLL
{
    public static class DependencyInjection
    {
        public static IServiceCollection AddServices(this IServiceCollection services)
        {
            services.AddTransient<IUserService, UserService>();
            services.AddTransient<IAccountService, AccountService>();
            return services;
        }
    }
}
