﻿using System;

namespace ZipAssessment.ExternalModels.V1
{
    public class AuditRecord
    {
        public string CreatedBy { get; set; }
        public DateTime Created { get; set; }

        public string LastModifiedBy { get; set; }
        public DateTime? LastModified { get; set; }

        public string DisabledBy { get; protected set; }
        public DateTime? Disabled { get; protected set; }
    }
}