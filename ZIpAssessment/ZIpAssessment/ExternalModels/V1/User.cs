﻿using System;
using ZipAssessment.BLL.Mapping;

namespace ZipAssessment.ExternalModels.V1
{
    public class User : AuditRecord, IMapFrom<BLL.InternalModels.User>
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public double MonthlySalary { get; set; }
        public double MonthlyExpenses { get; set; }

        public string EmailAddress { get; set; }
        public Guid ExternalId { get; set; }


    }
}
