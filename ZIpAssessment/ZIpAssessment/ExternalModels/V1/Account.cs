﻿using System;
using ZipAssessment.BLL.Mapping;

namespace ZipAssessment.ExternalModels.V1
{
    public class Account : AuditRecord, IMapFrom<BLL.InternalModels.Account>
    {
        public Guid ExternalId { get; set; }
        public double CurrentBalance { get; set; }
        public double Limit { get; set; }
        public bool AccountOverdue { get; set; }
    }
}