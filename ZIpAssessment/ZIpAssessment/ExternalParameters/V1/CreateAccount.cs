﻿using System;
using System.ComponentModel.DataAnnotations;

namespace ZipAssessment.ExternalParameters.V1
{
    public class CreateAccount
    {
        [Required]
        public Guid ExternalUserId { get; set; }
    }
}
