﻿using System.ComponentModel.DataAnnotations;
using ZipAssessment.BLL.Mapping;

namespace ZipAssessment.ExternalParameters.V1
{
    public class EditUser : IMapFrom<BLL.InternalParameters.EditUser>
    {
        [Required]
        public string FirstName { get; set; }
        [Required]
        public string LastName { get; set; }
        [Required]
        public double MonthlySalary { get; set; }
        [Required]
        public double MonthlyExpenses { get; set; }
        [Required, EmailAddress]
        public string EmailAddress { get; set; }
    }
}
