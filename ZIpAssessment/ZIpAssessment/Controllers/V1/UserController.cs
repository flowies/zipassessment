﻿using System;
using System.Linq;
using System.Net;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using AutoMapper.QueryableExtensions;
using Microsoft.AspNetCore.Mvc;
using ZipAssessment.BLL.Interfaces;
using ZipAssessment.ExternalModels.V1;
using ZipAssessment.ExternalParameters.V1;

namespace ZipAssessment.Controllers.V1
{
    [Route("api/[controller]")]//, ApiVersion("1")]
    [ApiController]
    public class UserController : ControllerBase
    {
        private readonly IUserService _userService;
        private readonly IMapper _mapper;

        public UserController(IUserService userService, IMapper mapper)
        {
            _userService = userService;
            _mapper = mapper;
        }


        // GET: api/User
        [HttpGet, ProducesResponseType(typeof(User[]), 200)]
        public async Task<IActionResult> GetAsync(CancellationToken cancellationToken)
        {
            return Ok((await _userService.GetAllUsers(cancellationToken)).AsQueryable().ProjectTo<User>(_mapper.ConfigurationProvider));
        }

        // GET: api/User/5
        [HttpGet("{id}", Name = "GetUserById"), ProducesResponseType(typeof(User), 200)]
        public async Task<User> Get(Guid id, CancellationToken cancellationToken)
        {
            return _mapper.Map<User>(await _userService.GetUserByExternalId(id, cancellationToken));
        }

        // POST: api/User
        [HttpPost, ProducesResponseType(typeof(User), 201)]
        public async Task<IActionResult> Post([FromBody] CreateUser value, CancellationToken cancellationToken)
        {
            var newUser = await _userService.CreateNewUser(_mapper.Map<CreateUser, BLL.InternalParameters.CreateUser>(value), cancellationToken);
            //mapping issue, so this is a hack as I am running out of spare time.
            var c = _mapper.Map<User>(await _userService.GetUserByExternalId(newUser.ExternalId, cancellationToken));
            return CreatedAtRoute("GetUserById", new { id = newUser.ExternalId }, c);
        }

        // PUT: api/User/5
        [HttpPut("{id}")]
        public async Task<IActionResult> Put(Guid id, [FromBody] EditUser value, CancellationToken cancellationToken)
        {
            return Ok(_mapper.Map<User>(await _userService.EditUser(id, _mapper.Map<BLL.InternalParameters.EditUser>(value), cancellationToken)));
        }

        // DELETE: api/ApiWithActions/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> Delete(Guid id, CancellationToken cancellationToken)
        {
            await _userService.DeleteUser(id, cancellationToken);
            return StatusCode((int)HttpStatusCode.NoContent);
        }
    }
}
