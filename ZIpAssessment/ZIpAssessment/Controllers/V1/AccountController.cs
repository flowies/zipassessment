﻿using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using AutoMapper.QueryableExtensions;
using Microsoft.AspNetCore.Mvc;
using ZipAssessment.BLL.Interfaces;
using ZipAssessment.ExternalModels.V1;
using ZipAssessment.ExternalParameters.V1;

namespace ZipAssessment.Controllers.V1
{
    [Route("api/[controller]")]
    [ApiController]
    public class AccountController : ControllerBase
    {
        private readonly IAccountService _accountService;
        private readonly IMapper _mapper;

        public AccountController(IAccountService accountService, IMapper mapper)
        {
            _accountService = accountService;
            _mapper = mapper;
        }
        // GET: api/Account
        [HttpGet, ProducesResponseType(typeof(Account[]), 200)]
        public async Task<IActionResult> GetAsync(CancellationToken cancellationToken)
        {
            return Ok((await _accountService.GetAllAccounts(cancellationToken)).AsQueryable().ProjectTo<Account>(_mapper.ConfigurationProvider));
        }

        // GET: api/Account/5
        [HttpGet("{id}", Name = "GetAccountById"), ProducesResponseType(typeof(Account), 200)]
        public async Task<Account> Get(Guid id, CancellationToken cancellationToken)
        {
            return _mapper.Map<Account>(await _accountService.GetAccountByExternalId(id, cancellationToken));
        }

        // POST: api/Account
        [HttpPost, ProducesResponseType(typeof(Account), 201)]
        public async Task<IActionResult> Post([FromBody] CreateAccount value, CancellationToken cancellationToken)
        {
            var newAccount = await _accountService.CreateNewAccount(value.ExternalUserId, cancellationToken);
            return CreatedAtRoute("GetAccountById", new { id = newAccount.Id }, _mapper.Map<Account>(newAccount));
        }


        // Edit and delete have been omitted as I expect complex business requirements
    }
}
